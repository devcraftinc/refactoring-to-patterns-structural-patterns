﻿// Copyright DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;

namespace ParkingPayment
{
    public class Pricing
    {
        public int CalculatePrice(DateTime entryTime, DateTime paymentTime)
        {
            if (paymentTime - entryTime <= TimeSpan.FromMinutes(10))
                return 0;

            var morning = entryTime.Date + TimeSpan.FromHours(6);
            if (morning > entryTime)
                morning -= TimeSpan.FromDays(1);

            var dailyPaymentStart = entryTime;
            var price = 0;

            while (dailyPaymentStart < paymentTime)
            {
                if (dailyPaymentStart.DayOfWeek >= DayOfWeek.Friday)
                {
                    var date = dailyPaymentStart.Date;
                    if (dailyPaymentStart < date + TimeSpan.FromHours(6))
                        date -= TimeSpan.FromDays(1);

                    var midpoint = date.Add(TimeSpan.FromHours(22));

                    var endOfHourlyParking = paymentTime > midpoint ? midpoint : paymentTime;
                    var timeInHourlyParking = endOfHourlyParking - dailyPaymentStart;

                    var dailyPrice = 0;
                    if (timeInHourlyParking > TimeSpan.Zero)
                    {
                        if (timeInHourlyParking <= TimeSpan.FromHours(1))
                            dailyPrice += 10;
                        else
                        {
                            var increments = (int) (1 + ((timeInHourlyParking - TimeSpan.FromHours(1)).Ticks - 1) /
                                                    TimeSpan.FromMinutes(15).Ticks);
                            dailyPrice += 10 + increments * 3;
                        }
                    }
 
                    if (paymentTime > midpoint)
                        dailyPrice += 40;

                    price += Math.Min(dailyPrice, 80);
                    morning = morning.AddDays(1);
                    dailyPaymentStart = morning;
                }
                else
                {
                    var nextMorning = morning.AddDays(1);
                    var endOfHourlyParking = paymentTime > nextMorning ? nextMorning : paymentTime;
                    var timeInHourlyParking = endOfHourlyParking - dailyPaymentStart;
                    int hourlyPrice;
                    if (timeInHourlyParking <= TimeSpan.FromHours(1))
                        hourlyPrice = 10;
                    else
                    {
                        var increments = (int)(1 + ((timeInHourlyParking - TimeSpan.FromHours(1)).Ticks - 1) /
                                               TimeSpan.FromMinutes(15).Ticks);
                        hourlyPrice = 10 + increments * 3;
                    }
                    price += Math.Min(hourlyPrice, 80);
                    morning = morning.AddDays(1);
                    dailyPaymentStart = morning;
                }
            } 

            return price;
        }
    }
}