﻿// Copyright DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ParkingPayment.Tests
{
  [TestClass]
  public class PricingTests
  {
    static readonly TimeSpan EveningThreshold = TimeSpan.FromHours(22);
    static readonly int InitialCost = 10;
    static readonly TimeSpan InitialBlockLength = TimeSpan.FromHours(1);
    static readonly TimeSpan GracePeriod = TimeSpan.FromMinutes(10);
    static readonly TimeSpan IncrementLength = TimeSpan.FromMinutes(15);
    static readonly int defaultIncrementCost = 3;
    static readonly int defaultWeekendEveningPrice = 40;
    static readonly int defaultMaximumDailyCost = 80;
    static readonly TimeSpan MorningThreshold = TimeSpan.FromHours(6);
    static readonly DateTime MondayAt7Am = TimeFrom(DayOfWeek.Monday, TimeSpan.FromHours(7));

    readonly Pricing pricing = new Pricing();

    [TestMethod]
    public void NoPaymentOnGracePeriod()
    {
        VerifyCostIs(MondayAt7Am, GracePeriod, 0);
    }

    [TestMethod]
    public void FirstHour()
    {
      VerifyCostIs(MondayAt7Am, JustAfter(GracePeriod), InitialCost);
      VerifyCostIs(MondayAt7Am, InitialBlockLength, InitialCost);
    }

    [TestMethod]
    public void FirstIncrement()
    {
        VerifyCostIs(MondayAt7Am, JustAfter(InitialBlockLength), InitialCost + defaultIncrementCost);
        VerifyCostIs(MondayAt7Am, InitialBlockLength + IncrementLength, InitialCost + defaultIncrementCost);
    }

    [TestMethod]
    public void NthIncrement()
    {
        VerifyCostIs(MondayAt7Am, JustAfter(InitialBlockLength + IncrementLength * 3), InitialCost + 4 * defaultIncrementCost);
        VerifyCostIs(MondayAt7Am, InitialBlockLength + IncrementLength * 4, InitialCost + 4 * defaultIncrementCost);
    }

    [TestMethod]
    public void MaximumDailyPrice()
    {
        VerifyCostIs(MondayAt7Am, InitialBlockLength + IncrementLength * 45, defaultMaximumDailyCost);
        VerifyCostIs(MondayAt7Am, InitialBlockLength.Add(TimeSpan.FromDays(1.5)), defaultMaximumDailyCost * 2);
        VerifyCostIs(MondayAt7Am, InitialBlockLength.Add(TimeSpan.FromDays(2.5)), defaultMaximumDailyCost * 3);

    }

    [TestMethod]
    public void MaximumDailyPriceOnWeekend()
    {
        VerifyCostIs(DayOfWeek.Friday, MorningThreshold, TimeSpan.FromHours(24), defaultMaximumDailyCost);
    }

        [TestMethod]
    public void WeekendEvening()
    {
      VerifyCostIs(DayOfWeek.Friday, EveningThreshold, JustAfter(GracePeriod), defaultWeekendEveningPrice);
      VerifyCostIs(DayOfWeek.Saturday, EveningThreshold, JustAfter(GracePeriod), defaultWeekendEveningPrice);
    }

    [TestMethod]
    public void WeekdayEvening()
    {
      VerifyCostIs(DayOfWeek.Sunday, EveningThreshold, JustAfter(GracePeriod), InitialCost);
      VerifyCostIs(DayOfWeek.Monday, EveningThreshold, JustAfter(GracePeriod), InitialCost);
      VerifyCostIs(DayOfWeek.Tuesday, EveningThreshold, JustAfter(GracePeriod), InitialCost);
      VerifyCostIs(DayOfWeek.Wednesday, EveningThreshold, JustAfter(GracePeriod), InitialCost);
      VerifyCostIs(DayOfWeek.Thursday, EveningThreshold, JustAfter(GracePeriod), InitialCost);
    }

    [TestMethod]
    public void WeekendDay()
    {
      VerifyCostIs(DayOfWeek.Friday, JustBefore(EveningThreshold - GracePeriod), JustAfter(GracePeriod), InitialCost);
    }

    [TestMethod]
    public void StraddleWeekendDayAndNight()
    {
      VerifyCostIs(DayOfWeek.Friday, JustBefore(EveningThreshold), JustAfter(GracePeriod), InitialCost + defaultWeekendEveningPrice);
    }

    [TestMethod]
    public void StraddleWeekendEveningAndMorning()
    {
      VerifyCostIs(DayOfWeek.Saturday, JustBefore(MorningThreshold), JustAfter(GracePeriod), InitialCost + defaultWeekendEveningPrice);
    }

    [TestMethod]
    public void LongStretchIntoWeekendNightStartingBeforeEvening()
    {
        VerifyCostIs(DayOfWeek.Friday, JustBefore(EveningThreshold), TimeSpan.FromHours(4), InitialCost + defaultWeekendEveningPrice);
    }

    [TestMethod]
    public void LateNightWeekendOutBeforeNewDayStarts()
    {
        VerifyCostIs(DayOfWeek.Friday, EveningThreshold, TimeSpan.FromHours(8), defaultWeekendEveningPrice);
    }

    [TestMethod]
    public void BreakIntoNextMorningOfWeekend()
    {
        var entryTime = TimeFrom(DayOfWeek.Friday, EveningThreshold);
        var paymentTime =  JustAfter(entryTime.AddDays(1).Date + MorningThreshold);
        VerifyCostIs(entryTime, paymentTime, InitialCost + defaultWeekendEveningPrice);
    }

    void VerifyCostIs(DayOfWeek dayOfWeek, TimeSpan entryTime, TimeSpan timeInParking, int expectedPrice)
    {
        var absoluteEntryTime = TimeFrom(dayOfWeek, entryTime);
        Assert.AreEqual(expectedPrice, CalculatePrice(absoluteEntryTime, absoluteEntryTime + timeInParking));
    }

    private void VerifyCostIs(DateTime entryTime, TimeSpan timeInParking, int expectedPrice)
    {
        VerifyCostIs(entryTime, entryTime + timeInParking, expectedPrice);
    }

    private void VerifyCostIs(DateTime entryTime, DateTime paymentTime, int expectedPrice)
    {
        Assert.AreEqual(expectedPrice, CalculatePrice(entryTime, paymentTime));
    }

    private int CalculatePrice(DateTime entryTime, DateTime paymentTime)
    {
        return pricing.CalculatePrice(entryTime, paymentTime);
    }

    private static DateTime TimeFrom(DayOfWeek dayOfWeek, TimeSpan offset)
    {
        var dateTime = DateTime.Now;
        while (dateTime.DayOfWeek != dayOfWeek)
            dateTime += TimeSpan.FromDays(1);
        var result = dateTime.Date + offset;
        return result;
    }

    static TimeSpan JustBefore(TimeSpan ts) => ts - TimeSpan.FromTicks(1);
    static TimeSpan JustAfter(TimeSpan ts) => ts + TimeSpan.FromTicks(1);
    static DateTime JustAfter(DateTime ts) => ts + TimeSpan.FromTicks(1);

    }
}